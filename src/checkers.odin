package main

import rl "vendor:raylib"
import "core:fmt"

Piece_Type :: enum u8 {
	Empty  = 0,
	Normal = 1,
	Queen  = 2,
}

Piece :: struct {
	type: Piece_Type,
	is_white: b8,
}

Board :: [8][4]Piece

Move :: struct {
	from: [2]i8,
	to:   [2]i8,
}

board: Board
next_move: Move
is_white_turn := true

init_board :: proc() {
	board = {
		{{.Normal, false}, {.Normal, false}, {.Normal, false}, {.Normal, false}},
		{{.Normal, false}, {.Normal, false}, {.Normal, false}, {.Normal, false}},
		{{.Normal, false}, {.Normal, false}, {.Normal, false}, {.Normal, false}},
		{},
		{},
		{{.Normal, true}, {.Normal, true}, {.Normal, true}, {.Normal, true}},
		{{.Normal, true}, {.Normal, true}, {.Normal, true}, {.Normal, true}},
		{{.Normal, true}, {.Normal, true}, {.Normal, true}, {.Normal, true}},
	};
}

process_move :: proc(using move: Move, white_turn := false, just_check := false, b: ^Board = nil) -> (value: int, ok: bool) {

	if move.from.x < 0 || move.from.x > 7 || move.from.y < 0 || move.from.y > 7 ||
	   move.to.x < 0 || move.to.x > 7 || move.to.y < 0 || move.to.y > 7
	{
		return
	}

	b := b
	if b == nil {
		b = &board
	}

	// Ckeck if there is a piece
	if b^[from.y][from.x / 2].type == .Empty {
		return
	}

	if move.from == move.to {
		return
	}

	is_white := bool(b^[from.y][from.x/2].is_white)

	is_queen := b^[from.y][from.x/2].type == .Queen

	if white_turn && !is_white ||
	  !white_turn && is_white 
	{
		return
	}

	// Check if to is valid
	if to.x > 7 || to.y > 7 || to.x < 0 || to.y < 0 {
		return
	}

	if b^[to.y][to.x/2].type != .Empty {
		return
	}

	if (to.x == from.x + 1              || to.x == from.x - 1) &&
	   (to.y == from.y + 1 && (!is_white || is_queen) || to.y == from.y - 1 && (is_white || is_queen))
	{
		if !just_check {
			piece := b^[from.y][from.x / 2]
			b^[from.y][from.x / 2].type = .Empty
			b^[to.y][to.x / 2] = piece 

			if is_white && to.y == 0 || !is_white && to.y == 7 {
				b^[to.y][to.x / 2].type = .Queen
				value = white_turn ? -1 : 1
			}
		}
	} else {
		if to.y == from.y - 2 || to.y == from.y + 2 {
			y_offset: i8 = to.y == from.y - 2 ? -1 : 1
			if to.x == from.x + 2 {
				to_piece := b^[from.y+y_offset][(from.x+1)/2]
				if to_piece.type != .Empty && to_piece.is_white != cast(b8)is_white {
					if !just_check {
						b^[from.y+y_offset][(from.x+1)/2].type = .Empty

						piece := b^[from.y][from.x / 2]
						b^[from.y][from.x / 2].type = .Empty
						b^[to.y][to.x / 2] = piece 

						value = white_turn ? -1 : 1

						if is_white && to.y == 0 || !is_white && to.y == 7 {
							value = white_turn ? -2 : 2
							b^[to.y][to.x / 2].type = .Queen
						}
					} else {
						value = white_turn ? -1 : 1
					}
				} else {
					return
				}
			} else if to.x == from.x - 2 {
				to_piece := b^[from.y+y_offset][(from.x-1)/2]
				if to_piece.type != .Empty && to_piece.is_white != cast(b8)is_white  {
					if !just_check {
						b^[from.y+y_offset][(from.x-1)/2].type = .Empty

						piece := b^[from.y][from.x / 2]
						b^[from.y][from.x / 2].type = .Empty
						b^[to.y][to.x / 2] = piece 

						value = white_turn ? -1 : 1

						if is_white && to.y == 0 || !is_white && to.y == 7 {
							value = white_turn ? -2 : 2
							b^[to.y][to.x / 2].type = .Queen
						}
					} else {
						value = white_turn ? -1 : 1
					}
				} else {
					return
				}
			} else {
				return
			}
		} else {
			return
		}
	}

	ok = true
	return	
}

check_possible_moves :: proc(from: [2]i8, white_turn: bool, b: ^Board = nil) -> (moves: [8]Move, count: int = 0, can_consume := false) {

	if from.x < 0 || from.x > 7 || from.y < 0 || from.y > 7 {
		return
	}

	b := b
	if b == nil {
		b = &board
	}

	dest := [8][2]i8 {
		{ 1, 1},
		{-1, 1},
		{ 1, -1},
		{-1, -1},
		{ 2, 2},
		{-2, 2},
		{ 2, -2},
		{-2, -2},
	}

	for i in 0..=7 {
		_, ok := process_move({from, dest[i] + from}, white_turn, true)
		if ok {
			// Force to consume a piece
			if i > 3 && !can_consume {
				count = 0
				can_consume = true
			}
			moves[count] = {from, dest[i] + from}
			count += 1
		}
	}

	return
}


ai_movment_rec :: proc(board: Board, depth: int = 0) -> (value: int) {
	if depth > max_depth {
		return
	}

	white_turn := depth % 2 == 0
	m_value: int = white_turn ? 9999 : -9999
	vcount: int = 0
	should_consume := false

	for y := 0; y < 8; y += 1 {
		offset: int = y % 2 == 0 ? 1 : 0
		for x := 0; x < 4; x += 1 {

			if board[y][x].type != .Empty && board[y][x].is_white == b8(white_turn) {
				move, count, can_consume := check_possible_moves({i8(x * 2 + offset), i8(y)}, white_turn)

				if can_consume && !should_consume {
					m_value = white_turn ? 9999 : -9999
					should_consume = true
				}

				if can_consume && should_consume || !should_consume {
					for m in move[:count] {
						b := board
						v, _ := process_move(m, white_turn, false, &b);

						if white_turn {
							if v < m_value {
								v += ai_movment_rec(b, depth + 1) 
								m_value = v
							}
						} else {
							if v > m_value {
								v += ai_movment_rec(b, depth + 1) 
								m_value = v
							}	
						}

						vcount += 1
					}
				}
			}
		}
	}

	if vcount > 0 {
		value = m_value
	}

	return
}


Move_Value :: struct {
	move: Move,
	value: int,
}


ai_movment :: proc() -> (out_move: Move) {
	moves: [512]Move_Value
	mcount: int
	b := board

	start_time := rl.GetTime()

	should_consume := false

	max_value: int = -9999

	for y := 0; y < 8; y += 1 {
		offset: int = y % 2 == 0 ? 1 : 0
		for x := 0; x < 4; x += 1 {
			if b[y][x].type != .Empty && !b[y][x].is_white {
				move, count, can_consume := check_possible_moves({i8(x * 2 + offset), i8(y)}, false)

				if can_consume && !should_consume {
					should_consume = true
					mcount = 0
				}

				if can_consume && should_consume || !should_consume {
					for m in move[:count] {
						b := board
						value, _ := process_move(m, false, false, &b);
						moves[mcount].value = value 
						moves[mcount].value += ai_movment_rec(b) 
						moves[mcount].move = m
						mcount += 1
					}
				}
			}
		}
	}

	for m in moves[:mcount] {
		if m.value > max_value {
			out_move = m.move
			max_value = m.value
		}		
	}

	fmt.println("value =", max_value, "time in s =", rl.GetTime() - start_time)

	return
}