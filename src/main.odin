package main

import "core:fmt"
import "core:strings"
import rl "vendor:raylib"

white_piece_normal_texture : rl.Texture
white_piece_queen_texture  : rl.Texture
black_piece_normal_texture : rl.Texture
black_piece_queen_texture  : rl.Texture
tile_green_texture         : rl.Texture
board_texture              : rl.Texture
circle_texture             : rl.Texture

// UI Textures
ui_panel_t           : rl.Texture
ui_button_left_t     : rl.Texture
ui_button_right_t    : rl.Texture
ui_button_new_game_t : rl.Texture

is_moving := false
mov_delay : f32 = 0

max_depth : int = 5

main :: proc() {
	rl.SetTraceLogLevel(.ERROR)
	rl.InitWindow(640 + 256, 640, "AI Checkers")

	// Load Textures
	{
		white_piece_normal_texture = rl.LoadTexture("assets/white_normal.png")
		white_piece_queen_texture  = rl.LoadTexture("assets/white_queen.png")
		black_piece_normal_texture = rl.LoadTexture("assets/black_normal.png")
		black_piece_queen_texture  = rl.LoadTexture("assets/black_queen.png")
		tile_green_texture         = rl.LoadTexture("assets/tile_green.png")

		board_texture  = rl.LoadTexture("assets/board.png")
		circle_texture = rl.LoadTexture("assets/circle.png")

		// Load UI Textures
		ui_panel_t           = rl.LoadTexture("assets/panel.png")
		ui_button_left_t     = rl.LoadTexture("assets/button_left.png");
		ui_button_right_t    = rl.LoadTexture("assets/button_right.png");
		ui_button_new_game_t = rl.LoadTexture("assets/new_game.png");
	}

	init_board();

	for !rl.WindowShouldClose() {

		mouse_pos_i: [2]i8
		should_consume := false

		if is_white_turn {
			for y : i8 = 0; y < 8; y += 1 {
				offset: i8 = y % 2 == 0 ? 1 : 0
				for x : i8 = 0; x < 4; x += 1 {
					moves, count, can_consume := check_possible_moves({x * 2 + offset, y}, is_white_turn)
					if can_consume {
						should_consume = true
					}
				}
			}
		}

		update_game : { // Update game
			mouse_pos := rl.GetMousePosition()
			mouse_pos /= 64
			mouse_pos_i = [2]i8{i8(mouse_pos.x), i8(mouse_pos.y)}
			mouse_pos_i -= 1

			if mov_delay > 0 {
				mov_delay -= rl.GetFrameTime()
				break update_game
			}
			
			if is_white_turn {	
				_, _, can_consume := check_possible_moves(mouse_pos_i, is_white_turn)
				if should_consume && can_consume || !should_consume {
					if rl.IsMouseButtonPressed(.LEFT) {
						if mouse_pos_i.y % 2 == 0 && mouse_pos_i.x % 2 == 1 ||
						   mouse_pos_i.y % 2 == 1 && mouse_pos_i.x % 2 == 0
						{
							next_move.from = mouse_pos_i
							is_moving = true
						}
					}
				}

				if rl.IsMouseButtonReleased(.LEFT) && is_moving {
					b := true
					is_moving = false
					next_move.to = mouse_pos_i

					if should_consume {
						value, ok := process_move(next_move, is_white_turn, true)
						if value == 0 {
							b = false
						}
					}

					if b {
						value, ok := process_move(next_move, is_white_turn)
						if ok {
							is_white_turn = !is_white_turn
						}
					}

					mov_delay = 0.25
				}
			} else {
				move := ai_movment()
				process_move(move, false)
				is_white_turn = !is_white_turn
			}
		}

		rl.BeginDrawing()

		rl.ClearBackground(rl.DARKBLUE)

		{ // Draw board 
			rl.DrawTextureEx(board_texture, {0, 0}, 0, 4, rl.WHITE)
		}

		{ // Draw UI
			rl.DrawTextureEx(ui_panel_t, {640, 0}, 0, 4, rl.WHITE)

			button :: proc(tex: rl.Texture, x, y, w, h: f32) -> (pressed: bool) {
				y_offset: f32

				mx := cast(f32)rl.GetMouseX()
				my := cast(f32)rl.GetMouseY()

				inside := mx > x && mx < (x + w) && my > y && my < (y + h)
				if inside {
					y_offset = -4

					if rl.IsMouseButtonDown(.LEFT) {
						y_offset = 4
					}

					if rl.IsMouseButtonReleased(.LEFT) {
						pressed = true
					}
				}

				rl.DrawTextureEx(tex, {x, y + y_offset}, 0, 4, rl.WHITE)

				return
			}

			if (button(ui_button_left_t, 672, 100, 32, 48)) {
				if max_depth > 1 {
					max_depth -= 1
				}
			}

			if (button(ui_button_right_t, 640 + 46 * 4, 100, 32, 48)) {
				if max_depth < 15 {
					max_depth += 1
				}
			}

			buf : [256]byte
			s := fmt.bprintf(buf[:255], "%d", max_depth)

			cs := strings.unsafe_string_to_cstring(s)

			rl.DrawText(cs, 640 + 30 * 4, 110, 24, {143, 87, 58, 255})
			rl.DrawText(cs, 640 + 30 * 4, 108, 24, {237, 194, 153, 255})

			if button(ui_button_new_game_t, 672, 46 * 4, 48 * 4, 48) {
				init_board()
				is_white_turn = true
			}
		}

		draw_piece :: proc(x, y: i32, is_white, is_queen: b8) {
			color  := rl.DARKGRAY
			color2 := rl.Color{color.r - 10, color.g - 10, color.b - 10, color.a} 

			xf := f32(x) - 32
			yf := f32(y) - 32

			if is_white {
				color = rl.WHITE
				color2 = rl.Color{color.r - 20, color.g - 20, color.b - 20, color.a} 
			}

			if is_white {
				if is_queen {
					rl.DrawTextureEx(white_piece_queen_texture, {xf, yf}, 0, 4, rl.WHITE)
				} else {
					rl.DrawTextureEx(white_piece_normal_texture, {xf, yf}, 0, 4, rl.WHITE)
				}
			} else {
				if is_queen {
					rl.DrawTextureEx(black_piece_queen_texture, {xf, yf}, 0, 4, rl.WHITE)
				} else {
					rl.DrawTextureEx(black_piece_normal_texture, {xf, yf}, 0, 4, rl.WHITE)
				}
			}
		}

		{ // Draw board
			for y: i32 = 0; y < 8; y += 1 {
				y_pos := y * 64  + 96;
				for x: i32 = 0; x < 4; x += 1 {
					x_pos := x * 128 + 96;

					offset: i32 = 0
					if y % 2 == 0 {
						x_pos += 64
						offset += 1
					}

					if board[y][x].type != .Empty {
						color  := rl.DARKGRAY
						color2 := rl.Color{color.r - 10, color.g - 10, color.b - 10, color.a} 

						if board[y][x].is_white {
							color = rl.WHITE
							color2 = rl.Color{color.r - 20, color.g - 20, color.b - 20, color.a} 
						}

						_, count, can_consume := check_possible_moves({i8(x*2 + offset), i8(y)}, is_white_turn)

						pi := is_moving ? next_move.from : mouse_pos_i
						if  count > 0 && pi == {i8(x*2 + offset), i8(y)} && (can_consume && should_consume || !should_consume) {
							rl.DrawTextureEx(circle_texture, {f32(x_pos - 32), f32(y_pos - 32)}, 0, 4, {10, 10, 10, 200})
							if !is_moving {	
								draw_piece(x_pos, y_pos - 4, board[y][x].is_white, board[y][x].type == .Queen)
							}
						} else {
							draw_piece(x_pos, y_pos, board[y][x].is_white, board[y][x].type == .Queen)
						}
					}
				}	
			}
		}

		if is_moving {	
			from := next_move.from;
			from.x = clamp(from.x, 0, 8)
			from.y = clamp(from.y, 0, 8)
			x := from.x/2;
			y := from.y;
			moves, count, can_consume := check_possible_moves(from, is_white_turn)

			if count > 0 {
				for m in moves[:count] {
					rl.DrawTextureEx(tile_green_texture, {f32(m.to.x) * 64 + 64, f32(m.to.y) * 64 + 64}, 0, 4, rl.WHITE)
				}

				draw_piece(rl.GetMouseX(), rl.GetMouseY(), board[y][x].is_white, board[y][x].type == .Queen)
			}
		}

		rl.EndDrawing()
	}

	rl.CloseWindow()	
}