# Checkers
---

### How to run
	- Open checkers_win.exe on windows
	- Open Checkers_linux on linux

The assets folder needs to be in the same directory as the executable

### How to compile

Go on [odin-lang.org](http://odin-lang.org/) and download the odin compiler

Run `odin build src -out:checkers -o:speed`